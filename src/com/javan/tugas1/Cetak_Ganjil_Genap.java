package com.javan.tugas1;

import java.io.Console;
import java.util.Scanner;

public class Cetak_Ganjil_Genap {

    private static Scanner myObj;

    public static void main(String[] args) {
        myObj  = new Scanner(System.in);
        oddEvenGenerator();
    }

    private static void oddEvenGenerator(){
        int A;
        int B;
        int i;
        try{
            System.out.print("A = ");
            A = myObj.nextInt();

            System.out.print("B = ");
            B = myObj.nextInt();

            if( A > B){
                System.out.format("Angka %d lebih dari %d, operasi tidak dapat dilakukan", A, B);
            }
            else{
                for(i = A; i <= B; i++){
                    if(i%2 == 0){
                        System.out.format("Angka %d adalah genap\n", i);
                    }
                    else
                        System.out.format("Angka %d adalah ganjil\n", i);
                }
            }
        }
        catch (Exception e){
            System.out.println("Harap masukkan angka");
        }
    }


}
