package com.javan.tugas1;

import java.util.ArrayList;
import java.util.Scanner;

public class HitungVokal {

    private static final ArrayList<String> listString = new ArrayList<>();
    private static final ArrayList<Character> listChar = new ArrayList<>();
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        int jumlahInput = 0;
        try {
            System.out.print("Jumlah input: ");
            jumlahInput = myObj.nextInt();
        }
        catch (Exception e){
            System.out.println("Wrong input format");
        }

        int i = 1;
        while (i <= jumlahInput){
            if(i == 1){
                System.out.format("%d. ", i);
                myObj.nextLine();
                listString.add(myObj.nextLine());
            }
            else{
                System.out.format("%d. ", i);
                listString.add(myObj.nextLine());
            }
            i++;
        }

        int k = 1;
        for (String s : listString) {
            listChar.clear();

            addList(s);

            if(listChar.isEmpty()){
                System.out.format("%d. \"%s\" = Tidak ada huruf vokal%n", k, s);
            }
            else{
                System.out.format("%n%d. \"%s\" = %d yaitu ", k, s, listChar.size());
                printHasil();
            }
            k++;
        }
    }

    public static void addList(String s){
        for(int j = 0; j < s.length(); j++){
            s = s.toLowerCase();
            char ch = s.charAt(j);
            if(!listChar.contains(ch) && (ch == 'a' || ch == 'i' || ch == 'u' || ch == 'e' || ch == 'o')){
                if(!listChar.contains(ch))
                    listChar.add(ch);
            }
        }
    }

    public static void printHasil(){
        int l = 0;
        for(char c : listChar){
            if(l == 0)
                System.out.print(c);
            if(l > 0 && l < listChar.size()-1)
                System.out.print(", "+c);
            if(l == listChar.size()-1)
                System.out.print(", dan "+c+"");
            l++;
        }
    }
}
