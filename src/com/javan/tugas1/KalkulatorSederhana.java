package com.javan.tugas1;

import java.util.ArrayList;
import java.util.Scanner;

public class KalkulatorSederhana {

    private static final ArrayList<String> listString = new ArrayList<>();

    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);

        int jumlahInput = 0;
        try {
            System.out.print("Jumlah input: ");
            jumlahInput = myObj.nextInt();
        }
        catch (Exception e){
            System.out.println("Wrong input format");
        }

        int i = 1;
        while (i <= jumlahInput){
            if(i == 1){
                System.out.format("%d. ", i);
                myObj.nextLine();
                listString.add(myObj.nextLine());
            }
            else{
                System.out.format("%d. ", i);
                listString.add(myObj.nextLine());
            }
            i++;
        }

        int j = 1;
        for(String s : listString){
            kalkulator(s, j);
            j++;
        }
    }

    private static void kalkulator(String input, int i){
        int hasil;
        int angkaPertama;
        int angkaKedua;
        String[] listS = input.split(" ");

        try {
            angkaPertama = Integer.parseInt(listS[0]);
            angkaKedua = Integer.parseInt(listS[2]);

            switch (listS[1]) {
                case "+":
                    hasil = angkaPertama + angkaKedua;
                    System.out.format("%d. %.1f\n", i, Float.parseFloat(String.valueOf(hasil)));
                    break;
                case "-":
                    hasil = angkaPertama - angkaKedua;
                    System.out.format("%d. %.1f\n", i, Float.parseFloat(String.valueOf(hasil)));
                    break;
                case "x":
                    hasil = angkaPertama * angkaKedua;
                    System.out.format("%d. %.1f\n", i, Float.parseFloat(String.valueOf(hasil)));
                    break;
                case "/":
                    try {
                        hasil = angkaPertama / angkaKedua;
                        System.out.format("%d. %.1f\n", i, Float.parseFloat(String.valueOf(hasil)));
                    } catch (Exception e) {
                        System.out.format("%d. tidak bisa dilakukan\n", i);
                    }
                    break;

                default:
                    System.out.format("%d. Format input salah. contoh benar: \"2 + 2\"\n", i);
            }
        }
        catch (Exception e){
            System.out.format("%d. Format input salah. contoh benar: \"2 + 2\"\n", i);
        }
    }
}
